
/**
 * @module test
 */
var test = test || {};
/**
 * @submodule a
 * @requires test
 * @static
 * @final
 */

test.a = test.a || {};

/**
 * 对象类
 * @class Subject
 * @extends Program  
 * @author 刘路
 * @version 0
 */
class Subject { 
 
    /**
     * 这是构造函数
     * @constructor 
     * @example
     *   var p = new Subject();
     */
    constructor(){  
        console.log('Subject');
        /**
         * @property id
         */
        this.id = Math.random(); 
        /**
        @property person
        @private
        @type String|Person|Object
        @default "123"
        */
        this.person = "123";
    }

    /**
     * @event onClick
     */
    onClick(){

    }

    /**
     * 这是一个方法
     * @method say
     * @param s {string} 虚拟参数
     * @return s {string} 返回值
     */
    say(){
        console.log('subject say '+this.id);
    }

    /**
     * @method dea
     * @deprecated
     */
    dea(){

    }
    /**
     * @method dess
     * @beta
     */
    dess(){

    }
} 
 

