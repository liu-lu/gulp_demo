
/**
 * 应用类
 * @class Program
 */
class Program extends Subject{

    /**
     * 构造应用的函数
     * @constructor
     */
    constructor(){
        super();
        console.log("Program");
        this.id = Math.random();
    }

    say(){
        console.log('program say '+this.id);
    }
} 

