// 包括了less预编译，
// css压缩，
// html文件include引入，
// js混淆压缩，
// 本地开发热刷新服务器，
// html压缩，
// 版本号添加

// http://snapsvg.io/docs/
// https://threejs.org/examples/
// https://threejs.org/docs/index.html#manual/introduction/Creating-a-scene

let gulp = require('gulp') ,                        //基础库
   
    babel = require('gulp-babel'), 
    uglify  = require('gulp-uglify'),               //js压缩
    autoprefixer = require('gulp-autoprefixer'),    // css文件 解决某些CSS属性不是标准属性，有各种浏览器前缀的情况
    /*
      gulp.src('src/images/*')
        .pipe(imagemin({
            progressive: true,
            use: [pngquant()] //使用pngquant来压缩png图片
        }))
        .pipe(gulp.dest('dist')); 
    */
    // imagemin 本身具有插件 目前不使用所以先这样
    imagemin = require('gulp-imagemin'),            //图片压缩
    // pngquant = require('imagemin-pngquant'), //png图片压缩插件 

    less = require('gulp-less'),                    // less
    // minifycss = require('gulp-minify-css'),         //css压缩
    minifycss = require('gulp-clean-css'),         //css压缩
    /*
    gulp.task('fileinclude', function() {  
    gulp.src(['index.html', 'home.html'])  
            .pipe(fileinclude({  
            prefix: '@@',  
            basepath: '@file'  
            }))  
        .pipe(gulp.dest('./'));  
    }); 
     */
    fileinclude  = require('gulp-file-include'),    // 文件包含
    jshint = require('gulp-jshint'),              //js检查
   
    rename = require('gulp-rename'),                //重命名
    concat  = require('gulp-concat'),             //合并文件 
    notify = require('gulp-notify'),                // 信息提示
    plumber = require('gulp-plumber'),      // 阻止错误退出 

    connect = require('gulp-connect'),              //本地开发web服务器，包括实时刷新

    rev = require('gulp-rev'),                         // 添加hash后缀
    revCollector  = require('gulp-rev-collector'),
    revReplace = require('gulp-rev-replace'),
    revAppend = require('gulp-rev-append'),

    sourceMap = require('gulp-sourcemaps'),

    minifyhtml= require('gulp-minify-html'),          // html 压缩
    // livereload = require('gulp-livereload');       // 使用connect代替

    md2pdf = require('gulp-markdown-pdf'),
    md2html = require('gulp-markdown'),

    git = require('gulp-git'),

    stripDebug = require('gulp-strip-debug'),

    jsdoc = require("gulp-jsdoc3"),
    yuidoc = require("gulp-yuidoc"),

    clean = require('gulp-clean')                      // 删除文件 
    ;

gulp.task('default',['clean','build']);

gulp.task('clean',function(){  
    gulp.src(['dist/*','build/*','docs/*'])
        .pipe(clean());
});

gulp.task('build',function(){
    gulp.src(['src/js/Subject.js','src/js/Program.js','src/main.js']) 
        .pipe(concat('fui.min.js'))
        .pipe(babel({
             presets: ['env']
         })) 
         .pipe(gulp.dest('build'))
         .pipe(uglify())
         .pipe(gulp.dest('dist'));
});

gulp.task('dist',function(){
     gulp.src("src/**/*.js")
         .pipe(notify("Hello Gulp!"));
});

gulp.task('concat', function () {
    gulp.src('src/**/*.js')  //要合并的文件
    .pipe(concat('fui.min.js'))  // 合并匹配到的js文件并命名为 "all.js"
    .pipe(gulp.dest('dist'));
});
  
gulp.task('css',function(){
    gulp.src(['src/**/*.css'])  //要压缩的css
        .pipe(autoprefixer())
        .pipe(minifycss())
        .pipe(gulp.dest('build'));
});

gulp.task('html',function(){
    gulp.src(['src/**/*.html'])  //要压缩的css
        .pipe(minifyhtml())
        .pipe(gulp.dest('build'));
});

gulp.task('check',function(){
    gulp.src(['src/**/*.js'])  //要压缩的css
        .pipe(jshint({
            esversion:6
        }))
        .pipe(jshint.reporter()); // 输出检查结果
});

gulp.task('less', function () {
    gulp.src('src/**/*.less')
        .pipe(less())
        .pipe(gulp.dest('dist'))
        .pipe(livereload());
});

gulp.task('watch', function() {
    livereload.listen(); //要在这里调用listen()方法
    gulp.watch('src/**/*.less', ['less']);  //监听目录下的文件，若文件发生变化，则调用less任务。
});

gulp.task('pdf',function(){
    gulp.src('src/**/*.md')
        .pipe(md2html()) // md2pdf
        .pipe(gulp.dest('docs'));
});

/** 使用connect启动一个Web服务器 **/
gulp.task('server', function () {
    connect.server({
        root: 'docs/docs/',
        livereload: true
    });
});


gulp.task('docs',function(){
    gulp.src('src/js/Subject.js')
        //.pipe(yuidoc())
        .pipe(jsdoc())
        .pipe(gulp.dest('docs')); 
}); 

 
// /*
// /** less编译 **/
// gulp.task('less', function () {
//     gulp.src('./src/less/mspei.less')
//         .pipe(less())
//         //.pipe(rename({ suffix: '.min' }))
//         //.pipe(minifycss())
//         .pipe(gulp.dest('./'+devDir+'/css/'));
// });

// /** js **/
// gulp.task('js', function () {
//     gulp.src(['./src/js/**/*'])
//         .pipe(gulp.dest('./'+devDir+'/js/'));
// });

// /** 通用文件包含 **/
// gulp.task('fileinclude', function() {
//     gulp.src(['./src/views/**/*.html', '!./src/views/include/**.html'])
//         .pipe(fileinclude({
//             prefix: '@@',
//             basepath: '@file'
//         }))
//         .pipe(gulp.dest('./'+devDir+'/'));
// });

// /** 图片处理 **/
// gulp.task('images', function () {
//     gulp.src(['./src/images/**/*'])
//         .pipe(gulp.dest('./'+devDir+'/images/'));
// });

// /** 字体图标 **/
// gulp.task('fonts', function () {
//     gulp.src(['./src/fonts/**/*'])
//         .pipe(gulp.dest('./'+devDir+'/fonts/'));
// });

// /** 使用connect启动一个Web服务器 **/
// gulp.task('connect', function () {
//     connect.server({
//         root: './'+devDir+'/',
//         livereload: true
//     });
// });

// /** 刷新页面 **/
// gulp.task('reload', function () {
//     gulp.src('./'+devDir+'/**/*.html')
//         .pipe(connect.reload());
// });

// /** 监测文件变动，设置自动执行的任务 **/
// gulp.task('watch', function () {
//     gulp.watch('./src/less/**/*.less', ['less', 'reload']);                   // 当所有less文件发生改变时，调用less任务
//     gulp.watch('./src/js/**/*.js', ['js', 'reload']);                   // 当所有js文件发生改变时，调用js任务
//     gulp.watch('./src/views/**/*.html', ['fileinclude', 'reload']); // 当所有模板文件变化时，重新生成生成页面到根目录
//     gulp.watch('./src/images/**/*', ['images']);                    // 监听images
// });

// /** 开发时，运行 'gulp dev' **/
// gulp.task('dev', ['connect', 'less', 'js', 'fileinclude', 'images', 'fonts', 'reload', 'watch']);



// /*****************************************************************
//  * dist版本，压缩版
//  ****************************************************************/
// /** less编译 **/
// gulp.task('lessDist', function () {
//     gulp.src('./src/less/mspei.less')
//         .pipe(less())
//         //.pipe(rename({ suffix: '.min' }))
//         .pipe(minifycss())
//         .pipe(rev())
//         .pipe(gulp.dest('./dist/css/'))
//         .pipe(rev.manifest())
//         .pipe(gulp.dest('./src/rev/css'));
// });

// /** js **/
// gulp.task('jsDist', function () {
//     gulp.src(['./src/js/**/*', '!./src/js/**/*.js'])
//         .pipe(gulp.dest('./dist/js'));
//     gulp.src(['./src/js/**/*.js'])
//         .pipe(uglify({
//             mangle: {except: ['require', 'exports', 'module', '$']}
//         }))
//         .pipe(rev())
//         .pipe(gulp.dest('./dist/js/'))
//         .pipe(rev.manifest())
//         .pipe(gulp.dest('./src/rev/js'));
// });

// /** 通用文件包含 **/
// gulp.task('fileincludeDist', function() {
//     gulp.src(['./src/views/**/*.html', '!./src/views/include/*.html'])
//         .pipe(fileinclude({
//             prefix: '@@',
//             basepath: '@file'
//         }))
//         .pipe(gulp.dest('./dist/'));
// });

// /** 图片处理 **/
// gulp.task('imagesDist', function () {
//     gulp.src(['./src/images/**/*'])
//         .pipe(gulp.dest('./dist/images/'));
// });

// /** 字体图标 **/
// gulp.task('fontsDist', function () {
//     gulp.src(['./src/fonts/**/*'])
//         .pipe(gulp.dest('./dist/fonts/'));
// });

// gulp.task('commonRev', function () {
//     return gulp.src(['src/rev/**/*.json', 'dist/**/*.html'])
//         .pipe( revCollector({
//             replaceReved: true,
//             dirReplacements: {
//                 'css': 'css',
//                 'js': 'js',
//                 //'cdn/': function(manifest_value) {
//                 //    return '//cdn' + (Math.floor(Math.random() * 9) + 1) + '.' + 'exsample.dot' + '/img/' + manifest_value;
//                 //}
//             }
//         }) )
//         .pipe( minifyHTML({
//             empty:true,
//             spare:true
//         }) )
//         .pipe( gulp.dest('dist') );
// });


// //处理seajs脚本的模块引用
// gulp.task("seaRev", function() {
//     var jsManifest = gulp.src('./src/rev/js/*.json');

//     return gulp.src(["src/js/config.js"], {
//             base: "src/js"
//         })
//         .pipe(revReplace({
//             manifest: jsManifest
//         }))
//         .pipe(rev())
//         .pipe(gulp.dest("dist/js"))
//         .pipe(rev.manifest("rev-manifest-seajs.json", {
//             merge: true
//         }))
//         .pipe(gulp.dest("src/rev/js"));
// });


// /** 发布时，运行 'gulp dist' **/
// /** 需版本号，再运行 'gulp rev' **/
// gulp.task('dist', ['lessDist', 'jsDist', 'fileincludeDist', 'imagesDist', 'fontsDist']);
// gulp.task('rev', ['commonRev', 'seaRev']);*/